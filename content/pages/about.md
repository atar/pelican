Title: About
Date: 01.01.2017
Category: About
Slug: about

# I'm Alexey. I like data.
![a bad man](../images/badman.jpg){:height="300px" width="300px" }
* * *
## Some facts
### Living
* Current: Edinburgh, Scotland
* Past: Dublin, Ireland. Moscow, Russia.

### Studies
* Current: High Performance Computing with Data Science. _Univeristy of Edinburgh_
* Past: Applied and Computational Mathematics. _University College Dublin_ (grad 2014)

### Work
* Current: Data Scientist. [_GSi_](http://surfaceintelligence.com). Cyclist [_Deliveroo_](https://deliveroo.co.uk)
* Past: Localisation QA analyst [_Keywords_](https://keywordsstudios.com).

### Interests
Music, cooking, bikes, fitness.
