Title: Contact
Date: 1.1.2017 18:19
Tags: contact, mail, pgp, riot, signal, xmpp
Category: Contact
Slug: contact
Summary:

I am reachable by the following means:

* Email: `alexey at tarut dot in`
* Signal: email the above for my number
* XMPP: `atar at dukgo dot com`
* Riot ID: `at a dot tar colon matrix dot org`
