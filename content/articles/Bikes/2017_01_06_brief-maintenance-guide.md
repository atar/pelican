Title: Brief maintenance guide
Date: 6.1.2017 15:15
Tags: bike, maintenance
Category: Bikes
Slug: brief-maintenance-guide
Summary:
[//]:<> (Status: draft)

I am not an authority on bikes. That title belongs to [Mr. Sheldon Brown](http://sheldonbrown.com/).

I do not work in a bike shop, nor do I have any training on any technical part of cycling.

I do, however, use my bike a lot. I cover about 100KM per week, cycling three times a week. From this usage,
my bike often requires cleaning and maintenance. 
