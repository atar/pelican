#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Alexey Tarutin'
SITENAME = AUTHOR
#SITEURL = 'http://tarut.in'
SITEURL = '/tarut.in'
SITELOGO = '/images/badman.jpg'
PYGMENTS_STYLE = 'monokai'

PATH = 'content'

TIMEZONE = 'Europe/London'
OG_LOCALE = "en_IE"
DEFAULT_LANG = "en"

DEFAULT_LANG = 'en'

THEME = 'theme'
#INDEX_SAVE_AS = '/pages/about.html'
#PAGE_SAVE_AS = '{slug}.html'
#PAGE_URL = '{slug}.html'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

DISPLAY_CATEGORIES_ON_MENU = False
DISPLAY_PAGES_ON_MENU = False

#INDEX_URL = '/pages/about.html'
MENUITEMS = (
        ('About', '/pages/about.html'),
        ('Tech', '/category/tech.html'),
        ('Bikes', '/category/bikes.html'),
        ('Sport', '/category/sport.html'),
        ('Food', '/category/food.html'),
        ('Music', '/category/music.html'),
        ('Misc', '/category/misc.html'),
        ('Contact', '/pages/contact.html')
        )

# Blogroll
#LINKS = (('Pelican', 'http://getpelican.com/'),
#         ('Python.org', 'http://python.org/'),
#         ('Jinja2', 'http://jinja.pocoo.org/'),
#         ('You can modify those links in your config file', '#'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
